module gitlab.com/toolhub/debugger

go 1.16

require (
	github.com/improbable-eng/grpc-web v0.14.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/rs/cors v1.8.0
	gitlab.com/toolhub/toolhub v0.0.0-20210902152616-f070914e12bf // indirect
	google.golang.org/grpc v1.40.0
)
