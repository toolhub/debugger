import {
	FunctionManager,
	ListFunctionRequest,
	ListFunctionResponse,
	FunctionInfo,
} from "@peymanmo/toolstudio-api";


var testFunction: FunctionInfo;

const manager = new FunctionManager();

function setStatus(text: string) {
	const element = document.getElementById('status');
	element.innerHTML = text;
}

window.addEventListener('DOMContentLoaded', async () => {
	setStatus("Connecting to server...");
	try {
		await manager.connect('/web');
	} catch (error) {
		setStatus(`Error: ${error}`);
		console.error(error);
		return;
	}

	setStatus("Getting functions...");
	try {
		const response = await manager.client.listFunctions(new ListFunctionRequest(), {});
		const items: ListFunctionResponse = response.getFunctionsList();
		items.forEach( (value: FunctionInfo) => {
			testFunction = value;
		});
	} catch (error) {
		setStatus(`Error: ${error}`);
		console.log(error);
		return;
	}

	const name = `${testFunction.getGroup()}/${testFunction.getName()}`
	setStatus(`OK (${name})`);

	manager.install(name, document.getElementById('function-viewport') as HTMLIFrameElement);
});
