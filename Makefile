.PHONY=generate
generate:
	protoc protocol/* --js_out=import_style=commonjs:web/src --grpc-web_out=import_style=typescript,mode=grpcweb:web/src

build-image:
	docker build -f docker/Dockerfile -t debugger .
