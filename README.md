# ToolStudio Debugger

ToolStudio tools and functions run in a unique condition.
There are CSP headers disallowing certain behavior, the URL of the
endpoint is not known at the time of coding and many other things.

In order to make it easy to design a tool and ascertain its correctness
and ability to run as a ToolStudio tool, one can use this tool.

## How to Use

This debugger is basically running an API that is hard wired to host and contain
a single function, your in-development function. A CDS that is hard-wired to host
only your static content without actually pulling any external files and a dispatcher
that only proxies the requests to your function.

The easiest way to use this is to add a `docker-compose.yml` file, like below:

```yaml
version: '3.0'

services:
  debugger:
    image: registry.gitlab.com/toolhub/debugger
    ports:
      - 9000:9000
      - 9001:9001
    environment:
      - "TOOLSTUDIO_FUNC_NAME=toolhub/image-resize"
      - "TOOLSTUDIO_FUNC_ENDPOINT=http://image-resize:4000/"
    volumes:
      - ./www:/var/www

  image-resize:
    build: .
```

Now if you have `thub` command line. You may use:

```sh
$ thub repo add debugger localhost:9001
$ thub repo sync debugger
$ thub func list
```
