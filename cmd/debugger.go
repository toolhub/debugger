package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/dispatcher"
	"gitlab.com/toolhub/toolhub/pkg/function"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type memoryAPI struct {
	functions map[string]*api.FunctionInfo
	endpoints map[string]*api.AccessEndpoint

	api.UnimplementedFunctionServiceServer
}

func (m *memoryAPI) ListFunctions(context.Context, *api.ListFunctionRequest) (*api.ListFunctionResponse, error) {
	funcs := make([]*api.FunctionInfo, 0, len(m.functions))
	for _, function := range m.functions {
		funcs = append(funcs, function)
	}
	return &api.ListFunctionResponse{Functions: funcs}, nil
}

func (m *memoryAPI) GetFunction(ctx context.Context, request *api.GetFunctionRequest) (*api.GetFunctionResponse, error) {
	name := fmt.Sprintf("%s/%s", request.Group, request.Name)
	if function, ok := m.functions[name]; ok {
		return &api.GetFunctionResponse{Function: function}, nil
	}
	return nil, grpc.Errorf(codes.NotFound, "function %s does not exist.", name)
}

func (m *memoryAPI) ListEndpoints(context.Context, *api.ListEndpointRequest) (*api.ListEndpointResponse, error) {
	return &api.ListEndpointResponse{Endpoints: m.endpoints}, nil
}

func startAPI(server *grpc.Server, config debuggerConfig) {
	listener, err := net.Listen("tcp", config.GRPCAddress)
	if err != nil {
		log.Fatalf("failed to start API service: %s", err)
	}

	parts := strings.Split(config.FunctionName, "/")

	service := &memoryAPI{
		endpoints: map[string]*api.AccessEndpoint{
			"*": {
				Insecure: true,
				Api: &api.RPCEndpoint{
					Grpc:    config.GRPCAddress,
					Gateway: fmt.Sprintf("%s/web", config.HTTPAddress),
				},
				Dispatcher: &api.RPCEndpoint{
					Http: fmt.Sprintf("%s/dispatcher", config.HTTPAddress),
				},
				Cds:       fmt.Sprintf("%s/cds", config.HTTPAddress),
				Dashboard: config.HTTPAddress,
			},
		},
		functions: map[string]*api.FunctionInfo{
			config.FunctionName: {
				Group: parts[0],
				Name:  parts[1],
				Status: &api.FunctionStatus{
					Status:           api.FunctionStatus_READY,
					DispatcherStatus: api.FunctionStatus_DISPATCHER_READY,
					FrontendStatus:   api.FunctionStatus_FRONTEND_READY,
				},
			},
		},
	}
	api.RegisterFunctionServiceServer(server, service)

	err = server.Serve(listener)
	log.Fatalf("API service failed: %s", err)
}

type debuggerConfig struct {
	HTTPAddress        string
	GRPCAddress        string
	FunctionName       string
	FunctionEndpoint   string
	FunctionStaticRoot string
}

func corsAllowAll(string) bool {
	return true
}

func addCSP(handler http.Handler, config debuggerConfig) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, req *http.Request) {
		urls := []string{
			fmt.Sprintf("http://localhost:9000/dispatcher/%s/evaluate/", config.FunctionName),
			fmt.Sprintf("http://127.0.0.1:9000/dispatcher/%s/evaluate/", config.FunctionName),
			fmt.Sprintf("http://0.0.0.0:9000/dispatcher/%s/evaluate/", config.FunctionName),
		}
		csp := fmt.Sprintf(
			"frame-src 'none'; object-src 'none'; connect-src %s; form-action 'none'; navigate-to 'none';",
			strings.Join(urls, " "))
		writer.Header().Add("Content-Security-Policy", csp)
		writer.Header().Add("X-Content-Security-Policy", csp)
		writer.Header().Add("X-Webkit-CSP", csp)
		handler.ServeHTTP(writer, req)
	})
}

func main() {
	config := debuggerConfig{
		HTTPAddress:        "0.0.0.0:9000",
		GRPCAddress:        "0.0.0.0:9001",
		FunctionStaticRoot: "/var/www/",
	}

	if addr := os.Getenv("TOOLSTUDIO_HTTP_ADDRESS"); len(addr) > 0 {
		config.HTTPAddress = addr
	}
	if addr := os.Getenv("TOOLSTUDIO_GRPC_ADDRESS"); len(addr) > 0 {
		config.GRPCAddress = addr
	}
	if value := os.Getenv("TOOLSTUDIO_FUNC_STATIC_ROOT"); len(value) > 0 {
		config.FunctionStaticRoot = value
	}
	config.FunctionName = os.Getenv("TOOLSTUDIO_FUNC_NAME")
	config.FunctionEndpoint = os.Getenv("TOOLSTUDIO_FUNC_ENDPOINT")

	grpcServer := grpc.NewServer()
	go startAPI(grpcServer, config)

	parts := strings.Split(config.FunctionName, "/")
	repo := function.NewMemoryRepository()
	repo.Add(function.Descriptor{
		Group:    parts[0],
		Name:     parts[1],
		Endpoint: config.FunctionEndpoint,
	})

	// Allow any origin so long as they ask for POST or HEAD only.
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
		},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: false,
		ExposedHeaders:   []string{"*", "Content-Disposition"},
	})

	mux := http.NewServeMux()

	// Dispatcher
	router := httprouter.New()
	router.Handler("POST", "/dispatcher/:group/:name/evaluate/", dispatcher.NewServer(repo))
	mux.Handle("/dispatcher/", router)

	// gRPC API
	options := []grpcweb.Option{grpcweb.WithOriginFunc(corsAllowAll), grpcweb.WithAllowNonRootResource(true)}
	wrappedGrpc := grpcweb.WrapServer(grpcServer, options...)
	mux.Handle("/web/", wrappedGrpc)

	// CDS
	cdsPath := fmt.Sprintf("/cds/%s/", config.FunctionName)
	cdsHandler := http.FileServer(http.Dir(config.FunctionStaticRoot))
	mux.Handle(cdsPath, addCSP(http.StripPrefix(cdsPath, cdsHandler), config))

	// Debugger
	mux.Handle("/", http.FileServer(http.Dir("web/dist")))

	log.Fatalln(http.ListenAndServe(config.HTTPAddress, c.Handler(mux)))
}
