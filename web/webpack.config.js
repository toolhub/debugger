var path = require('path');

module.exports = {
  mode: "production",
  entry: "./src/debug.ts",
	resolve: {
		extensions: ['.ts', '.js'],
	},
	module: {
		rules: [
			{test: /\.ts$/, loader: 'ts-loader'}
		],
	},
	output: {
		filename: 'debug.js',
		path: path.resolve(__dirname, 'dist'),
	},
};
