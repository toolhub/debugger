#!/usr/bin/env sh
ARCH=$(uname -m)
DOWNLOAD_ARCH=
DOWNLOAD_URL=

case "$ARCH" in
	"arm") DOWNLOAD_ARCH=arm
		;;
	arm*) DOWNLOAD_ARCH=arm64
		;;
	"i386") DOWNLOAD_ARCH=i386
		;;
	"i686") DOWNLOAD_ARCH=i386
		;;
	"x86_64") DOWNLOAD_ARCH=amd64
		;;
esac

DOWNLOAD_URL=https://github.com/upx/upx/releases/download/v3.96/upx-3.96-${DOWNLOAD_ARCH}_linux.tar.xz
echo Downloading $DOWNLOAD_URL
wget -O upx.tar.xz $DOWNLOAD_URL
tar -Jx -f upx.tar.xz --strip-components 1
cp upx /bin/upx
